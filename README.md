Fravega Challenge 1
====================

Para mejorar la experiencia del cliente, la sucursal default que se ofrece para store pickup debe ser la más
cercana a su ubicación. Para esto, una de las funcionalidades que se necesita es conocer la sucursal más cercana
a un punto dado (latitud y longitud).

Diseñar e implementar un servicio que exponga en su API las operaciones CRUD (únicamente creación y
lectura por id) de la entidad Sucursal y la consulta de sucursal más cercana a un punto dado.

Los campos de dicha entidad son id , dirección , latitud y longitud


## Instalación

```bash
# clone el repositorio a un directorio de su agrado
git clone git@gitlab.com:german.gutierrez/fravegachallengespring.git  [my-checkedout-repository] 
```



Ejecute los siguientes comandos de docker-compose
```bash
cd [my-checkedout-repository]
docker-compose build
docker-compose up -d
```

Ejecute los siguientes comandos de docker-compose para detener y reiniciar servicios
```bash
cd [my-checkedout-repository]
docker-compose stop maven
docker-compose stop mysql
docker-compose start maven # se ejecuta el script entrypoint que ejecuta mvn spring-boot:run
docker-compose start mysql
```


Ejecutar comandos en los contenedores
```
 docker exec -ti fravega-spring mvn test # ejecutar test
 docker exec -ti fravega-spring mvn clean # limpiar 
 echo "select now() as hoy" | docker exec -i fravegaspring-mysql mysql  -u fravegatest -pfravegatest fravegatest
 docker exec -it fravegaspring-mysql mysql  -u fravegatest -pfravegatest fravegatest  # ingresar al shell de mysql
 docker exec -ti fravega-spring /bin/bash # ingesar a la terminal de maven



```



# Documentación de la API
## Crear una sucursal

`POST http://{host}:{port}/sucursal`

Ejemplo request

```shell
curl -H "Content-Type: application/json" \
	-d '{"direccion": "Av. Siempre viva 123","longitud":-34.123132,"latitud":-36.546546}' \
	-X POST http://localhost:8080/sucursal

```

Ejemplo response exito
```json
{
  "statusCode" : "CREATED",
  "data" : {
    "id" : null,
    "direccion" : "Av. Siempre viva 123",
    "longitud" : -34.123132,
    "latitud" : -36.546546
  }
}
```


Ejemplo response error (http 503)
```json
{
  "cause" : null,
  "stackTrace" : [ ],
  "message" : "Ya existe una registro para esa dirección",
  "suppressed" : [ ],
  "localizedMessage" : "Ya existe una registro para esa dirección"
}
```
## Detalle de sucursal por id

`GET http://{host}:{port}/sucursal/{id}`

Ejemplo request

```shell
curl -H "Content-Type: application/json" http://localhost:8080/sucursal/1	

```

Ejemplo response
```json
{
  "statusCode" : "OK",
  "data" : {
    "id" : 1,
    "direccion" : "Av. Siempre viva 123",
    "longitud" : -34.123132,
    "latitud" : -36.546546
  }
}
```

Ejemplo response error (http 503)
```json
{
  "cause" : null,
  "stackTrace" : [ ],
  "message" : "No existe sucursal con ese id",
  "suppressed" : [ ],
  "localizedMessage" : "No existe sucursal con ese id",
}
```


## Listado de surcusales

`GET http://{host}:{port}/sucursal`

Ejemplo request
```shell
curl -H "Content-Type: application/json" http://localhost:8080/sucursal
```

Ejemplo response
```json
{
    "statusCode" : "OK",
    "data" :[
        {
        "id" : 1,
        "direccion" : "Av. Siempre viva 123",
        "longitud" : -34.123132,
        "latitud" : -36.546546
        }
    ]
        
    
}
```

## Buscar sucursal cercana a coordenadas

`POST http://{host}:{port}/sucursal`

Ejemplo request

```shell
curl -H "Content-Type: application/json" \
	-d '{"longitud":-34.123132,"latitud":-36.546546}' \
	-X POST http://localhost:8080/sucursal/getnear

```


Ejemplo response
```json
{
  "statusCode" : "OK",
  "data" : {
    "id" : 1,
    "direccion" : "Av. Siempre viva 123",
    "longitud" : -34.123132,
    "latitud" : -36.546546
  }
}
```


## healthcheck

`GET http://{host}:{port}/healthcheck

Ejemplo request

```shell
curl -H "Content-Type: application/json" http://localhost:8080/healthcheck

```

Ejemplo response
```json
{
  "service" : "up",  
}
```



Ejemplo error
```json
{
  "service" : "down",  
}
```