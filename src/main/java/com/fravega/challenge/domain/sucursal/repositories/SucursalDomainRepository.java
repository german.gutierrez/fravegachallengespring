package com.fravega.challenge.domain.sucursal.repositories;

import com.fravega.challenge.domain.sucursal.Sucursal;
import com.fravega.challenge.domain.sucursal.Sucursal;
import java.util.List;
import javax.persistence.Query;
import org.springframework.data.jpa.repository.JpaRepository;




/**
 *
 * @author Germán Gutierrez <german.gutierrez@snoopconsulting.com>
 */
public interface SucursalDomainRepository  {
    
    /**
     * verifica conexion
     * @return boolean
     */
    public boolean checkHealh();

    public Sucursal getNear(Double longitud, Double latitud);

    
    public Sucursal findSucursal(Long id);

    public Sucursal createSucursal(String direccion, Double longitud, Double latitud);

    public  void deleteSucursales();
    List<Sucursal> findAllSucursales();
}
