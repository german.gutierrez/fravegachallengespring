/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fravega.challenge.domain.sucursal.services;

import com.fravega.challenge.domain.sucursal.exceptions.SucursalNotFoundException;
import com.fravega.challenge.domain.sucursal.exceptions.SucursalDataIntegrityViolationException;
import com.fravega.challenge.domain.BusinessRequest.SucursalBusinessRequest;
import com.fravega.challenge.infraestructure.Persistance.SucursalRepository;
import com.fravega.challenge.domain.sucursal.Sucursal;
import com.fravega.challenge.domain.sucursal.Sucursal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.PersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Germán Gutierrez <german.gutierrez@snoopconsulting.com>
 */
@Service
public class SucursalService {
    
    @Autowired SucursalRepository repository;
  

    public boolean checkHealh() {
        
        return repository.checkHealh();
        
    }
    
    public boolean delete() {
        repository.deleteSucursales();
        return true;
        
    }


    public List<Sucursal> findAll() {
        
        List<Sucursal> sucursales = repository.findAllSucursales();
         if(sucursales.size() > 0) {
            return sucursales;
        } else { 
            return new ArrayList<Sucursal>();
        }
    }

    
    /**
     * 
     */
    public Sucursal create(SucursalBusinessRequest item) throws SucursalDataIntegrityViolationException {
        try{
        Sucursal sucursal = repository.createSucursal(item.direccion, item.longitud, item.latitud);
        return sucursal;
         } catch (org.springframework.dao.DataIntegrityViolationException e ){
            throw new SucursalDataIntegrityViolationException(Sucursal.SUCURSAL_DIRECCION_ERROR);
        }
        
    }

    
    /**
     * finds sucursal or throws exception
     * @param id
     * @return
     * @throws SucursalNotFoundException 
     */
    public Sucursal find(Long id) throws SucursalNotFoundException {
        
        try{
        Sucursal item = repository.findSucursal(id);
        
            return item;    
        } catch(Exception NoResultException){
            throw new SucursalNotFoundException(Sucursal.SUCURSAL_NOTFOUND);
        }
        
        
        
    }

    
    
    public Sucursal getnear(SucursalBusinessRequest item) {
        
           Sucursal nearStore = repository.getNear(item.longitud, item.latitud);
            return nearStore;        
        
    }
}
