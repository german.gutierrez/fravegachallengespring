/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fravega.challenge.domain.sucursal;

import java.math.BigInteger;
import java.util.Optional;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Germán Gutierrez <german.gutierrez@snoopconsulting.com>
 */
@Entity
@Table(name="sucursales")
public class Sucursal {
    
    public static final Double MIN_LONGITUDE_VALUE = -180.0;
    public static final Double MAX_LONGITUDE_VALUE = 180.0;
    public static final Double MIN_LATITUDE_VALUE = -90.0;
    public static final Double MAX_LATITUDE_VALUE = 90.0;
    public static final String SUCURSAL_CREATED ="Sucursal creada";
    public static final String SUCURSAL_DIRECCION_EXIST ="Ya existe una sucursal con esa direccion";
    public static final String SUCURSAL_OUTOFBOUNDS_ERROR = "Valores fuera del rango [-180,180] para longitud o [-90,90] para latitud";
    public static final String SUCURSAL_NOTFOUND ="No existe sucursal con ese id";
    public static final String SUCURSAL_DIRECCION_ERROR ="Ya existe una registro para esa dirección";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", unique=true, nullable = false)
    private Long id;
    private String direccion;
    
    
    private Double longitud; 
    
    
    private Double latitud;

    public Sucursal(Long id, String direccion, Double longitud, Double latitud) {
        
        this.id = id;        
        this.direccion = direccion;
        this.longitud = longitud;
        this.latitud = latitud;
    }
    
    public Sucursal(String direccion, Double longitud, Double latitud) {
        
            
        this.direccion = direccion;
        this.longitud = longitud;
        this.latitud = latitud;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    
    public Double getLongitud() {
        return longitud;
    }

    
    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    
    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }
    
   /**
    * verifica el valor de longitud no esta dentro de los rangos  -180 180
    *
    * @param Double value
    * @return Boolean
    */
    public static Boolean isNotValidSucursalLongitude(Double value) {
        return value < Sucursal.MIN_LONGITUDE_VALUE
                || value > Sucursal.MAX_LONGITUDE_VALUE;
        
    }

    /**
    * verifica el valor de latitud no esta dentro de los rangos  -90 90
    *
    * @param Double value
    * @return Boolean
    */
    public static Boolean isNotValidSucursalLatitude(Double value) {
        return value < Sucursal.MIN_LATITUDE_VALUE 
                || value > Sucursal.MAX_LATITUDE_VALUE;
    }

    @Override
    public String toString() {
        return "Sucursal{" + "id=" + id + ", direccion=" + direccion + ", longitud=" + longitud + ", latitud=" + latitud + '}';
    }
    
    
    
}
