package com.fravega.challenge.application.controllers;


import com.fravega.challenge.domain.BusinessResponse.BusinessResponse;
import com.fravega.challenge.domain.BusinessResponse.BusinessResponseList;
import com.fravega.challenge.domain.BusinessRequest.SucursalBusinessRequest;
import com.fravega.challenge.domain.sucursal.exceptions.SucursalDataIntegrityViolationException;
import com.fravega.challenge.domain.sucursal.exceptions.SucursalNotFoundException;
import com.fravega.challenge.domain.sucursal.services.SucursalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fravega.challenge.domain.sucursal.Sucursal;
import java.util.HashMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
/**
 *
 * @author Germán Gutierrez <german.gutierrez@snoopconsulting.com>
 */
@RestController
public class SucursalController {
    @Autowired SucursalService service;
    
   
    
    @GetMapping("/sucursal")
    @ResponseBody    
     public BusinessResponseList  getAll(){
        return new BusinessResponseList(service.findAll(), HttpStatus.OK);	
     }
     
     
    @PostMapping("/sucursal")
    @ResponseBody    
     public BusinessResponse  create(@RequestBody SucursalBusinessRequest item) throws SucursalDataIntegrityViolationException{
         
        return new BusinessResponse(service.create(item), HttpStatus.CREATED);	
     }
    @PostMapping("/sucursal/getnear")
    @ResponseBody    
     public BusinessResponse getnear(@RequestBody SucursalBusinessRequest item) {
         
        return new BusinessResponse(service.getnear(item),HttpStatus.OK);
     }
     
        
    @DeleteMapping(value="/sucursal/delete")
    @ResponseBody
    public ResponseEntity<String> delete()  {
        service.delete();
        return new ResponseEntity<String>("deleted", new HttpHeaders(), HttpStatus.OK);
    }
     
    @GetMapping(value="/sucursal/{id}", produces="application/json")
    @ResponseBody
    public BusinessResponse get(@PathVariable Long id) throws SucursalNotFoundException {
        return new BusinessResponse(service.find(id),HttpStatus.OK);
    }
    
 
}
