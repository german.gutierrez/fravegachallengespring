/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fravega.challenge.application.controllers;

import com.fravega.challenge.domain.sucursal.services.SucursalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.HashMap;

/**
 *
 * @author Germán Gutierrez <german.gutierrez@snoopconsulting.com>
 */
@RestController
public class HealthCheckController {
    @Autowired SucursalService service;
    
    @GetMapping("/healthcheck")
    @ResponseBody    
     public ResponseEntity<HashMap<String, String>> getAll(){
         HashMap<String, String> health = new HashMap<String, String>();

         
         if(service.checkHealh()){
             health.put("service", "up");
         } else {
             health.put("service", "down");
         }
        return new ResponseEntity<HashMap<String, String>>(health, new HttpHeaders(), HttpStatus.OK);	
     }
}
