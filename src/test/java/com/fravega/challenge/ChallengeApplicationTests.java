package com.fravega.challenge;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ChallengeApplicationTests {

	@Test
	void contextLoads() {
	}
        
        
        public static void main(String[] args) {
		SpringApplication.run(ChallengeApplicationTests.class, args);
	}

}
